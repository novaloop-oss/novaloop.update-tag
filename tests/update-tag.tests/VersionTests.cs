using Xunit;
using Version = Application.Models.Version;

namespace UpdateTag.Tests
{
    public class VersionTests
    {
        [Theory]
        [InlineData(1, 0, 0, "", "", "2.0.0")]
        [InlineData(1, 1, 0, "", "", "2.0.0")]
        [InlineData(1, 1, 1, "", "", "2.0.0")]
        public void NextMajor(int major, int minor, int patch, string rc, string service, string expected)

        {
            var version = new Version(major, minor, patch, rc, service, false).NextMajor();
            Assert.Equal(expected, version.ToString());
        }


        [Theory]
        [InlineData(1, 0, 0, "", "", "1.1.0")]
        [InlineData(1, 1, 0, "", "", "1.2.0")]
        [InlineData(1, 1, 1, "", "", "1.2.0")]
        public void NextMinor(int major, int minor, int patch, string rc, string service, string expected)

        {
            var version = new Version(major, minor, patch, rc, service, false).NextMinor();
            Assert.Equal(expected, version.ToString());
        }

        [Theory]
        [InlineData(1, 0, 0, "", "", "1.0.1")]
        [InlineData(1, 1, 0, "", "", "1.1.1")]
        [InlineData(1, 1, 1, "", "", "1.1.2")]
        public void NextPatch(int major, int minor, int patch, string rc, string service, string expected)

        {
            var version = new Version(major, minor, patch, rc, service, false).NextPatch();
            Assert.Equal(expected, version.ToString());
        }

        [Theory]
        [InlineData(1, 1, 1, "RC.4", "", "1.1.1-RC.5")]
        [InlineData(1, 1, 1, "RC.4", "ErpNext", "1.1.1-RC.5+ErpNext")]
        public void NextRc(int major, int minor, int patch, string rc, string service, string expected)

        {
            var version = new Version(major, minor, patch, rc, service, false).NextRc();
            Assert.Equal(expected, version.ToString());
        }

        [Theory]
        [InlineData(1, 1, 1, null, "", "1.1.2-RC.0")]
        [InlineData(1, 1, 1, null, "ErpNext", "1.1.2-RC.0+ErpNext")]
        public void CreatePatchRc(int major, int minor, int patch, string rc, string service, string expected)
        {
            var version = new Version(major, minor, patch, rc, service, false).CreatePatchRc();
            Assert.Equal(expected, version.ToString());
        }

        [Theory]
        [InlineData(1, 1, 1, null, "", "1.2.0-RC.0")]
        [InlineData(1, 1, 1, null, "ErpNext", "1.2.0-RC.0+ErpNext")]
        public void CreateMinorRc(int major, int minor, int patch, string rc, string service, string expected)
        {
            var version = new Version(major, minor, patch, rc, service, false).CreateMinorRc();
            Assert.Equal(expected, version.ToString());
        }

        [Theory]
        [InlineData(1, 1, 1, null, "", "2.0.0-RC.0")]
        [InlineData(1, 1, 1, null, "ErpNext", "2.0.0-RC.0+ErpNext")]
        public void CreateMajroRc(int major, int minor, int patch, string rc, string service, string expected)
        {
            var version = new Version(major, minor, patch, rc, service, false).CreateMajorRc();
            Assert.Equal(expected, version.ToString());
        }

        [Theory]
        [InlineData(1, 1, 1, "RC.4", "", "1.1.1")]
        [InlineData(1, 1, 1, "RC.4", "ErpNext", "1.1.1+ErpNext")]
        public void ReleaseRc(int major, int minor, int patch, string rc, string service, string expected)
        {
            var version = new Version(major, minor, patch, rc, service, false).ReleaseRc();
            Assert.Equal(expected, version.ToString());
        }
    }
}