using System.Collections.Generic;
using System.Threading;
using Application.Interfaces;
using Application.Models;
using Application.Queries;
using Moq;
using Xunit;

namespace UpdateTag.Tests
{
    public class GetVersionInformationFromRepoTests
    {
        [Fact]
        public async void DoesReadCurrentVersionCorrectly()
        {
            // Arrange
            var mockedVersionList = new List<Version>
            {
                new Version(0, 1, 5, false),
                new Version(0, 1, 7, false),
                new Version(0, 2, 0, false),
                new Version(0, 2, 0, 0, false),
                new Version(0, 2, 0, 1, false),
                new Version(0, 2, 0, 2, false)
            };
            var gitRepoMock = new Mock<IGitRepoReadService>();
            gitRepoMock.Setup(m => m.GetAllVersions(It.IsAny<string>()))
                .Returns(mockedVersionList);
            var handler = new GetVersionInformationFromRepo(gitRepoMock.Object);
            var query = new GetVersionInformationFromRepo.Query("");

            // Act
            var versionInformation = await handler.Handle(query, CancellationToken.None);

            // Assert
            Assert.Equal("0.2.0", versionInformation.CurrentVersion.ToString());
        }

        [Theory]
        [InlineData(0, 2, 0, true, "v0.2.0")]
        [InlineData(0, 2, 0, false, "0.2.0")]
        public async void AddVPrefixToNextVersionIfCurrentVersionHasOne(int major, int minor, int patch, bool hasVPrefix,
            string expectedVersionOutput)
        {
            // Arrange
            var mockedVersionList = new List<Version>
            {
                new Version(major, minor, patch, hasVPrefix)
            };
            var gitRepoMock = new Mock<IGitRepoReadService>();
            gitRepoMock.Setup(m => m.GetAllVersions(It.IsAny<string>()))
                .Returns(mockedVersionList);
            var handler = new GetVersionInformationFromRepo(gitRepoMock.Object);
            var query = new GetVersionInformationFromRepo.Query("");

            // Act
            var versionInformation = await handler.Handle(query, CancellationToken.None);

            // Assert
            Assert.Equal(expectedVersionOutput, versionInformation.CurrentVersion.ToString());
        }
    }
}