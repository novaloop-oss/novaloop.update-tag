using System.Linq;
using Application.Models;
using Xunit;

namespace UpdateTag.Tests
{
    public class VersionInformationTests
    {
        [Fact]
        public void CorrectNextVersionsFromRc()

        {
            // Arrange
            var version = new Version(1, 0, 1, 1, false);

            // Act
            var versionInformation = new VersionInformation(version);
            var versions = versionInformation.NextVersions.Select(nv => nv.Version.ToString()).ToList();

            // Assert
            Assert.Contains("1.0.1-RC.2", versions); // next rc
            Assert.Contains("1.0.1", versions); // release rc
            Assert.Equal(2, versions.Count);
        }

        [Fact]
        public void CorrectNextVersionsFromNonRc()

        {
            // Arrange
            var version = new Version(1, 0, 1, false);

            // Act
            var versionInformation = new VersionInformation(version);
            var versions = versionInformation.NextVersions.Select(nv => nv.Version.ToString()).ToList();


            // Assert
            Assert.Contains("1.0.2-RC.0", versions); // patch RC
            Assert.Contains("1.1.0-RC.0", versions); // minor RC
            Assert.Contains("2.0.0-RC.0", versions); // major RC
            Assert.Contains("1.0.2", versions); // next patch
            Assert.Contains("1.1.0", versions); // next minor
            Assert.Contains("2.0.0", versions); // next major
            Assert.Equal(6, versions.Count);
        }
    }
}