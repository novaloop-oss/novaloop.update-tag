#!/usr/bin/env bash
dotnet pack src -o ../../local-nuget-packages
unset -v latest
for file in "../local-nuget-packages"/*; do
  [[ $file -nt $latest ]] && latest=$file
done
echo "$latest"
dotnet nuget push "$latest" -k "$NUGET_API_KEY" -s https://api.nuget.org/v3/index.json