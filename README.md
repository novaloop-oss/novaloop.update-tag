# update-tag

Updates the tag of a repo to the next chosen version according the semver symantic.

## Install

```
dotnet tool install --global Novaloop.UpdateTag
```

## Update

```
dotnet tool update --global Novaloop.UpdateTag
```