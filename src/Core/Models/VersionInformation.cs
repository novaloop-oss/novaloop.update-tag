using System.Collections.Generic;

namespace Application.Models
{
    public class VersionInformation
    {
        public VersionInformation(Version currentVersion)
        {
            CurrentVersion = currentVersion;
            if (currentVersion.IsRc())
            {
                NextVersions.Add(new NextVersion("next rc", currentVersion.NextRc()));
                NextVersions.Add(new NextVersion("release", currentVersion.ReleaseRc()));
            }
            else
            {
                NextVersions.Add(new NextVersion("patch-rc", currentVersion.CreatePatchRc()));
                NextVersions.Add(new NextVersion("minor-rc", currentVersion.CreateMinorRc()));
                NextVersions.Add(new NextVersion("major-rc", currentVersion.CreateMajorRc()));
                NextVersions.Add(new NextVersion("patch   ", currentVersion.NextPatch()));
                NextVersions.Add(new NextVersion("minor   ", currentVersion.NextMinor()));
                NextVersions.Add(new NextVersion("major   ", currentVersion.NextMajor()));
            }
        }

        public Version CurrentVersion { get; }

        public List<NextVersion> NextVersions = new List<NextVersion>();
    }

    public class NextVersion
    {
        public NextVersion(string title, Version version)
        {
            Title = title;
            Version = version;
        }

        public string Title { get; }
        public Version Version { get; }
    }
}