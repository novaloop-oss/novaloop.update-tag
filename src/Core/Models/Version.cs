using System;
using System.Text;
using System.Text.RegularExpressions;

namespace Application.Models
{
    /// <summary>
    /// Construct:
    /// "Major.Minor.Patch-Rc+Service"
    /// 
    /// Example:
    /// "0.1.4-RC.4+ErpNext"
    /// </summary>
    public class Version
    {
        public Version(int major, int minor, int patch, bool hasVPrefix)
        {
            Major = major;
            Minor = minor;
            Patch = patch;
            _hasVPrefix = hasVPrefix;
        }

        public Version(int major, int minor, int patch, int? rc, bool hasVPrefix)
        {
            Major = major;
            Minor = minor;
            Patch = patch;
            Rc = rc;
            _hasVPrefix = hasVPrefix;
        }

        public Version(int major, int minor, int patch, string rc, string service, bool hasVPrefix)
        {
            Major = major;
            Minor = minor;
            Patch = patch;
            Rc = rc == null ? null : ExtractNumberFromRcString(rc);
            Service = service ?? "";
            _hasVPrefix = hasVPrefix;
        }

        public Version(int major, int minor, int patch, int? rc, string service, bool hasVPrefix)
        {
            Major = major;
            Minor = minor;
            Patch = patch;
            Rc = rc;
            Service = service ?? "";
            _hasVPrefix = hasVPrefix;
        }

        private static int? ExtractNumberFromRcString(string rc)
        {
            var resultString = Regex.Match(rc, @"\d+").Value;
            return int.TryParse(resultString, out var rcNumber) ? rcNumber : null;
        }


        public int Major { get; private set; }
        public int Minor { get; private set; }
        public int Patch { get; private set; }
        public int? Rc { get; private set; }
        public string Service { get; private set; }
        private readonly bool _hasVPrefix;

        public override string ToString()
        {
            var sb = new StringBuilder();
            if (_hasVPrefix)
            {
                sb.Append('v');
            }

            sb.Append(Major);
            sb.Append('.');
            sb.Append(Minor);
            sb.Append('.');
            sb.Append(Patch);

            if (Rc != null)
            {
                sb.Append('-');
                sb.Append("RC.");
                sb.Append(Rc);
            }

            if (Service != null && Service.Length > 0)
            {
                sb.Append('+');
                sb.Append(Service);
            }

            return sb.ToString();
        }

        private Version Copy()
        {
            return new Version(Major, Minor, Patch, Rc, Service, _hasVPrefix);
        }

        public Version NextMajor()
        {
            if (Rc != null)
            {
                throw new ArgumentException("Cannot create next Major. Release RC first.");
            }

            var nextVersion = Copy();
            nextVersion.Major++;
            nextVersion.Minor = 0;
            nextVersion.Patch = 0;
            return nextVersion;
        }

        public Version NextMinor()
        {
            if (Rc != null)
            {
                throw new ArgumentException("Cannot create next Minor. Release RC first.");
            }

            var nextVersion = Copy();
            nextVersion.Minor++;
            nextVersion.Patch = 0;
            return nextVersion;
        }

        public Version NextPatch()
        {
            if (Rc != null)
            {
                throw new ArgumentException("Cannot create next Patch. Release RC first.");
            }

            var nextVersion = Copy();
            nextVersion.Patch++;
            return nextVersion;
        }

        public Version NextRc()
        {
            if (Rc == null)
            {
                throw new ArgumentException("Cannot create next RC. Not an RC.");
            }

            var nextVersion = Copy();
            nextVersion.Rc++;
            return nextVersion;
        }

        public Version CreatePatchRc()
        {
            if (Rc != null)
            {
                throw new ArgumentException("Cannot create RC. Already an RC.");
            }

            var nextVersion = Copy();
            nextVersion.Patch++;
            nextVersion.Rc = 0;
            return nextVersion;
        }

        public Version CreateMinorRc()
        {
            if (Rc != null)
            {
                throw new ArgumentException("Cannot create RC. Already an RC.");
            }

            var nextVersion = Copy();
            nextVersion.Minor++;
            nextVersion.Patch = 0;
            nextVersion.Rc = 0;
            return nextVersion;
        }

        public Version CreateMajorRc()
        {
            if (Rc != null)
            {
                throw new ArgumentException("Cannot create RC. Already an RC.");
            }

            var nextVersion = Copy();
            nextVersion.Major++;
            nextVersion.Minor = 0;
            nextVersion.Patch = 0;
            nextVersion.Rc = 0;
            return nextVersion;
        }

        public Version ReleaseRc()
        {
            if (Rc == null)
            {
                throw new ArgumentException("Cannot release RC. Not an RC.");
            }

            var nextVersion = new Version(Major, Minor, Patch, (string)null, Service, _hasVPrefix);
            return nextVersion;
        }

        public bool IsRc()
        {
            return Rc != null;
        }

        public void SetService(string service)
        {
            Service = service;
        }
    }
}