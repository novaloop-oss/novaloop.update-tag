using System.Threading;
using System.Threading.Tasks;

namespace Application.Interfaces
{
    public interface IGitRepoWriteService
    {
        void AddTag(string repoPath, string tag);
        Task<string> PushTags(string repoPath, CancellationToken cancellationToken);
        Task<string> Push(string repoPath, CancellationToken ct);
    }
}