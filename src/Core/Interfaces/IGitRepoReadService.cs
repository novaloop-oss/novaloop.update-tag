using System.Collections.Generic;
using System.Threading.Tasks;
using Application.Models;

namespace Application.Interfaces
{
    public interface IGitRepoReadService
    {
        IEnumerable<Version> GetAllVersions(string repoPath);
        Task<string> GetRepoBasePath(string workingDir);
    }
}