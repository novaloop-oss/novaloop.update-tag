using System.Threading;
using System.Threading.Tasks;
using Application.Interfaces;
using MediatR;

namespace Application.Queries
{
    public class PushTagsToRemote : IRequestHandler<PushTagsToRemote.Command, string>
    {
        private readonly IGitRepoWriteService _gitRepoWriteService;

        public PushTagsToRemote(IGitRepoWriteService gitRepoWriteService)
        {
            _gitRepoWriteService = gitRepoWriteService;
        }

        public class Command : IRequest<string>
        {
            public string RepoPath { get; }

            public Command(string repoPath)
            {
                RepoPath = repoPath;
            }
        }


        public async Task<string> Handle(Command request, CancellationToken ct)
        {
            return await _gitRepoWriteService.PushTags(request.RepoPath, ct);
        }
    }
}