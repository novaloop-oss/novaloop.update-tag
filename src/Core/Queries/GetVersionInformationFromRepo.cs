﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Application.Interfaces;
using Application.Models;
using MediatR;

namespace Application.Queries
{
    public class GetVersionInformationFromRepo : IRequestHandler<GetVersionInformationFromRepo.Query, VersionInformation>
    {
        public class Query : IRequest<VersionInformation>
        {
            public Query(string repositoryPath, string onlyForService = "")
            {
                RepositoryPath = repositoryPath;
                OnlyForService = onlyForService;
            }

            public string RepositoryPath { get; }
            public string OnlyForService { get; }
        }

        private readonly IGitRepoReadService _gitRepoReadService;

        public GetVersionInformationFromRepo(IGitRepoReadService gitRepoReadService)
        {
            _gitRepoReadService = gitRepoReadService;
        }

        public async Task<VersionInformation> Handle(Query request, CancellationToken cancellationToken)
        {
            var versions = _gitRepoReadService
                .GetAllVersions(request.RepositoryPath);

            if (!string.IsNullOrWhiteSpace(request.OnlyForService))
            {
                versions = versions
                    .Where(v => v.Service.Equals(request.OnlyForService, StringComparison.InvariantCultureIgnoreCase));
            }

            var currentVersion = versions
                .OrderByDescending(v => v.Major)
                .ThenByDescending(v => v.Minor)
                .ThenByDescending(v => v.Patch)
                .ThenByDescending(v => v.Rc == null)
                .ThenByDescending(v => v.Rc)
                .FirstOrDefault();

            return await Task.FromResult(currentVersion == null ? null : new VersionInformation(currentVersion));
        }
    }
}