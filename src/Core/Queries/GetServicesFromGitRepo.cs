﻿using System.Collections.Generic;
using System.Linq;
using Application.Interfaces;
using MediatR;

namespace Application.Queries
{
    public class GetServicesFromGitRepo : RequestHandler<GetServicesFromGitRepo.Query, List<string>>
    {
        public class Query : IRequest<List<string>>
        {
            public Query(string repositoryPath)
            {
                RepositoryPath = repositoryPath;
            }

            public string RepositoryPath { get; }
        }

        private readonly IGitRepoReadService _gitRepoReadService;

        public GetServicesFromGitRepo(IGitRepoReadService gitRepoReadService)
        {
            _gitRepoReadService = gitRepoReadService;
        }

        protected override List<string> Handle(Query request)
        {
            return _gitRepoReadService
                .GetAllVersions(request.RepositoryPath)
                .Select(v => v.Service)
                .Distinct()
                .OrderBy(v => v)
                .ToList();
        }
    }
}