﻿using System.Threading;
using System.Threading.Tasks;
using Application.Interfaces;
using MediatR;

namespace Application.Queries
{
    public class GetRepoBasePath : IRequestHandler<GetRepoBasePath.Query, string>
    {
        public class Query : IRequest<string>
        {
            public Query(string workingDir)
            {
                WorkingDir = workingDir;
            }

            public string WorkingDir { get; }
        }

        private readonly IGitRepoReadService _gitRepoReadService;

        public GetRepoBasePath(IGitRepoReadService gitRepoReadService)
        {
            _gitRepoReadService = gitRepoReadService;
        }


        public async Task<string> Handle(Query request, CancellationToken cancellationToken)
        {
            return await _gitRepoReadService.GetRepoBasePath(request.WorkingDir);
        }
    }
}