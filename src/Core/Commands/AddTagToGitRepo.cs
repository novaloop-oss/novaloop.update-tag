using Application.Interfaces;
using MediatR;

namespace Application.Commands
{
    public class AddTagToGitRepo : RequestHandler<AddTagToGitRepo.Command>
    {
        private readonly IGitRepoWriteService _gitRepoWriteService;

        public AddTagToGitRepo(IGitRepoWriteService gitRepoWriteService)
        {
            _gitRepoWriteService = gitRepoWriteService;
        }

        public class Command : IRequest
        {
            public string RepoPath { get; }
            public string Tag { get; }


            public Command(string repoPath, string tag)
            {
                RepoPath = repoPath;
                Tag = tag;
            }
        }


        protected override void Handle(Command request)
        {
            _gitRepoWriteService.AddTag(request.RepoPath, request.Tag);
        }
    }
}