using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Application.Interfaces;
using CliWrap;
using CliWrap.Buffered;
using LibGit2Sharp;
using Semver;
using Version = Application.Models.Version;

namespace Infrastructure.Services
{
    public class GitRepoReadService : IGitRepoReadService
    {
        public async Task<string> GetRepoBasePath(string workingDir)
        {
            var result = await Cli.Wrap("git")
                .WithArguments("rev-parse --show-toplevel")
                .WithWorkingDirectory(workingDir)
                .ExecuteBufferedAsync();
            return result.StandardOutput.Trim();
        }

        public IEnumerable<Version> GetAllVersions(string repoPath)
        {
            using var repo = new Repository(repoPath);
            foreach (var tag in repo.Tags)
            {
                if (TryParse(tag.FriendlyName, out var semver))
                {
                    yield return semver;
                }
            }
        }

        private static bool TryParse(string versionStr, out Version version)
        {
            try
            {
                var semver = SemVersion.Parse(versionStr.TrimStart('v').TrimStart('V'));
                var hasVPrefix = versionStr.StartsWith("v", StringComparison.InvariantCultureIgnoreCase);
                version = new Version(semver.Major, semver.Minor, semver.Patch, semver.Prerelease, semver.Build, hasVPrefix);
                return true;
            }
            catch
            {
                version = null;
                return false;
            }
        }
    }
}