using System.Threading;
using System.Threading.Tasks;
using Application.Interfaces;
using CliWrap;
using CliWrap.Buffered;
using LibGit2Sharp;

namespace Infrastructure.Services
{
    public class GitRepoWriteService : IGitRepoWriteService
    {
        public void AddTag(string repoPath, string tag)
        {
            using var repo = new Repository(repoPath);
            repo.ApplyTag(tag);
        }

        public async Task<string> PushTags(string repoPath, CancellationToken ct)
        {
            var result = await Cli.Wrap("git")
                .WithArguments("push --tags")
                .WithWorkingDirectory(repoPath)
                .ExecuteBufferedAsync(ct);
            return result.StandardOutput.Trim();
        }

        public async Task<string> Push(string repoPath, CancellationToken ct)
        {
            var result = await Cli.Wrap("git")
                .WithArguments("push")
                .WithWorkingDirectory(repoPath)
                .ExecuteBufferedAsync(ct);

            return result.StandardOutput.Trim();
        }
    }
}