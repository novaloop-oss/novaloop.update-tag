﻿using System;
using System.Threading.Tasks;
using Application;
using Cli.Models;
using CommandLine;
using Infrastructure;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace Cli
{
    class Program
    {
        static async Task Main(string[] args)
        {
            using var host = CreateHostBuilder()
                .Build();

            var appRunner = host
                .Services
                .CreateScope()
                .ServiceProvider
                .GetRequiredService<AppRunner>();

            await Parser.Default
                .ParseArguments<CliParams>(args)
                .WithParsedAsync(
                    async options => { await appRunner.Run(options.RepositoryPath ?? Environment.CurrentDirectory); }
                );
        }


        private static IHostBuilder CreateHostBuilder()
        {
            return Host.CreateDefaultBuilder()
                .ConfigureServices(
                    (_, services) =>
                        services
                            .AddCli()
                            .AddInfrastructure()
                            .AddCore()
                );
        }
    }
}