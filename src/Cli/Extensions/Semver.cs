using Semver;

namespace Cli.Extensions
{
    public static class SemverParser
    {
        public static bool TryParse(string version, out SemVersion semverVersion)
        {
            try
            {
                semverVersion = SemVersion.Parse(version.TrimStart('v').TrimStart('V'));
                return true;
            }
            catch
            {
                semverVersion = null;
                return false;
            }
        }
    }
}