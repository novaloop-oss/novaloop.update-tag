using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Application.Commands;
using Application.Queries;
using Cli.Models;
using MediatR;
using Spectre.Console;
using Version = Application.Models.Version;

namespace Cli
{
    public class AppRunner
    {
        private readonly IMediator _mediator;

        public AppRunner(IMediator mediator)
        {
            _mediator = mediator;
        }

        public async Task Run(string workingDir)
        {
            var repoBasePath = await GetRepoBasePath(workingDir);
            var chosenService = await ChooseService(repoBasePath);
            var selection = await SelectVersion(repoBasePath, chosenService);

            await AnsiConsole.Status()
                .StartAsync("pushing to remote...", async _ =>
                    {
                        await AddVersionTagToRepo(repoBasePath, selection.Version.ToString());
                        await PushTagsToRemote(repoBasePath);
                        await PushCommitsToRemote(repoBasePath);
                    }
                );
        }


        private async Task<string> GetRepoBasePath(string workingDir)
        {
            var repoBasePath = "";
            try
            {
                repoBasePath = await _mediator.Send(new GetRepoBasePath.Query(workingDir));
            }
            catch (CliWrap.Exceptions.CommandExecutionException)
            {
                AnsiConsole.Markup("[red]Error:[/] Unable to extract Versions. Are we running inside a git repository?\n\n");
                Environment.Exit(1);
            }

            return repoBasePath;
        }

        private async Task<string> ChooseService(string repoBasePath)
        {
            var services = new List<string>();
            try
            {
                services = await _mediator.Send(new GetServicesFromGitRepo.Query(repoBasePath));
            }
            catch (LibGit2Sharp.RepositoryNotFoundException)
            {
                AnsiConsole.Markup("[red]Error:[/] Unable to extract Versions. Are we running inside a git repository?\n\n");
                Environment.Exit(1);
            }

            var chosenService = "";
            if (services.Count > 1)
            {
                chosenService = AnsiConsole.Prompt(
                    new SelectionPrompt<string>()
                        .Title("Which [green]service[/] is the new version for?")
                        .PageSize(10)
                        .MoreChoicesText("[grey](Move up and down to reveal more services)[/]")
                        .AddChoices(services));
            }

            return chosenService;
        }

        private async Task<Selection> SelectVersion(string repoBasePath, string chosenService)
        {
            var versionInfo = await _mediator.Send(new GetVersionInformationFromRepo.Query(repoBasePath, chosenService));
            Selection selection;
            if (versionInfo != null)
            {
                selection = AnsiConsole.Prompt(
                    new SelectionPrompt<Selection>()
                        .Title($"Select new version. (Current version is [green]{versionInfo.CurrentVersion}[/])")
                        .PageSize(10)
                        .AddChoices(
                            versionInfo
                                .NextVersions
                                .Select(nv => new Selection(nv.Title, nv.Version))
                                .ToList()
                        )
                );
            }
            else
            {
                selection = AnsiConsole.Prompt(
                    new SelectionPrompt<Selection>()
                        .Title("[red]Error evaluating version from newest tag.[/]\nAdd new version tag **AND** push to origin?)")
                        .PageSize(10)
                        .AddChoices(
                            new Selection("yes", new Version(0, 1, 0, true)),
                            new Selection("yes", new Version(0, 1, 0, 1, true)),
                            new Selection("no", null)
                        )
                );
                var serviceName = AnsiConsole.Prompt(
                    new TextPrompt<string>("[grey][[Optional]][/] Enter [green]service name[/]:")
                        .AllowEmpty()
                );

                if (!string.IsNullOrWhiteSpace(serviceName))
                {
                    selection.Version.SetService(serviceName.Trim());
                }
            }

            if (selection.Version == null)
            {
                Environment.Exit(0);
            }

            return selection;
        }

        private async Task PushTagsToRemote(string workingDir)
        {
            try
            {
                var output = await _mediator.Send(new PushTagsToRemote.Command(workingDir));
                AnsiConsole.Write(output);
            }
            catch (Exception ex)
            {
                AnsiConsole.Markup("[red]Error:[/] Tag was written but unable to push to remote\n\n");
                AnsiConsole.WriteException(ex);
                Environment.Exit(1);
            }
        }

        private async Task PushCommitsToRemote(string workingDir)
        {
            try
            {
                var output = await _mediator.Send(new PushCommitsToRemote.Command(workingDir));
                AnsiConsole.Write(output);
            }
            catch (Exception ex)
            {
                AnsiConsole.Markup("[red]Error:[/] Tag was written but unable to push to remote\n\n");
                AnsiConsole.WriteException(ex);
                Environment.Exit(1);
            }
        }

        private async Task AddVersionTagToRepo(string workingDir, string tag)
        {
            try
            {
                await _mediator.Send(new AddTagToGitRepo.Command(workingDir, tag));
            }
            catch (Exception ex)
            {
                AnsiConsole.Markup("[red]Error:[/] Unable to write Tag to repository\n\n");
                AnsiConsole.WriteException(ex);
                Environment.Exit(1);
            }
        }
    }
}