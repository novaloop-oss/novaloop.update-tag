using Microsoft.Extensions.DependencyInjection;

namespace Cli
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddCli(this IServiceCollection services)
        {
            return services
                .AddTransient<AppRunner>();
        }
    }
}