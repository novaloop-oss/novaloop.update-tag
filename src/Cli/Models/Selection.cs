using Application.Models;

namespace Cli.Models
{
    public class Selection
    {
        public Selection(string title, Version version)
        {
            Title = title;
            Version = version;
        }

        private string Title { get; }
        public Version Version { get; }

        public override string ToString()
        {
            return Version == null ? Title : $"{Title}\t [green]{Version}[/]";
        }
    }
}