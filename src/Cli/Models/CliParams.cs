using CommandLine;

namespace Cli.Models
{
    public class CliParams
    {
        [Option('r', "repository-path", Required = false, HelpText = "Run update-tag on a git repository other than the current directory.")]
        public string RepositoryPath { get; set; }
    }
}